﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class WeaponAmmoContainer : MonoBehaviour
    {
        public abstract void FireAmmo(ItemWeaponRanged owner, Vector3 direction, float speed);
    }
}