﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class Item : MonoBehaviour
    {
        public int itemId;
        public ItemTag itemTag;
        public Sprite icon;
        public int stackMaxAmount = -1;

        protected EquipItem owner;

        public event Action<Item> OnConsumed;

        public abstract void Initialize();

        public virtual void PickUp(EquipItem owner) 
        {
            this.owner = owner; 
        }

        protected virtual void OnItemConsumed()
        {
            OnConsumed?.Invoke(this);
        }
        public virtual void Use() { }
        public virtual void AltUse() { }
        public virtual void Cancel() { }
    }
}