﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class ItemUseableCancelable : ItemUseable
    {
        [SerializeField] protected UnityEventContainer onCancelUseEvents;
        public event System.Action OnCancel;
        public override void Cancel() { OnItemCancelUse(); OnCancel?.Invoke(); onCancelUseEvents.unityEvents?.Invoke(); }
        protected abstract void OnItemCancelUse();
    }
}