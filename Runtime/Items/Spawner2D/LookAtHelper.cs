﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomInventory
{
    public class LookAtHelper : MonoBehaviour
    {
        [SerializeField] private Transform lookAtTarget = null;
        [SerializeField] private Transform defaultPosition = null;
        [SerializeField] private Vector3 offset = default;

        public bool isFollowing { get; set; }
        public Vector3 Position { get; set; }

        private void Update()
        {
            if (isFollowing)
                lookAtTarget.position = Position;
            else
                ResetPosition();
        }

        public void ResetPosition()
        {
            if (defaultPosition == null)return;
            lookAtTarget.position = defaultPosition.TransformPoint(offset);
        }
    }
}