﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using DigitomPhysics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInventory
{
    public class Spawner2D : ItemAltUseableCancelable
    {
        protected enum AxisType { X, Y, Z }

        [SerializeField] protected InputActionReference pointerInput = null;
        [SerializeField] protected GameObject correctPlacementPrefab = null;
        [SerializeField] protected GameObject incorrectPlacementPrefab = null;
        [SerializeField] protected GameObject spawnPrefab = null;
        [SerializeField] protected OverlapBox3DSettings overlapSettings = default;
        [SerializeField] protected bool enableGridSnap = false;
        [SerializeField] protected float gridSize = 0;
        [SerializeField] protected AxisType rotateAxis = default;
        [SerializeField] protected float rotateAmount = 0;
        [SerializeField] protected bool unselectAllOnIncorrectPlacement;
        [SerializeField] protected bool unselectAllOnCorrectPlacement;

        protected Vector3 placementPosition;
        protected Quaternion placementRotation;
        private GameObject correctSpawn;
        private GameObject incorrectSpawn;

        private Camera cam;
        private LookAtHelper lookAtHelper;

        private IPhysicsDetectable3D overlapSystem;

        private InputAction input;
        private Vector2 pointerPosition;

        private void Awake()
        {
            overlapSystem = new OverlapBox3D(transform, overlapSettings);
            lookAtHelper = FindObjectOfType<LookAtHelper>();
        }

        private void OnEnable()
        {
            cam = Camera.main;
            if (input != null)
                input.Enable();
            if (lookAtHelper != null)
                lookAtHelper.isFollowing = true;
        }

        private void OnDisable()
        {
            if (correctSpawn)
                Destroy(correctSpawn);
            if (incorrectSpawn)
                Destroy(incorrectSpawn);
            if (lookAtHelper != null)
                lookAtHelper.isFollowing = false;
        }

        private void Update()
        {
            ReadInputPosition();
            ConvertInputToWorld();
            SetHelperPosition();
        }

        protected virtual void FixedUpdate()
        {
            overlapSystem.PositionOverride = placementPosition;
            overlapSystem.Tick();
            CheckPlacement();
        }

        public override void PickUp(EquipItem owner)
        {
            base.PickUp(owner);
            var pi = this.owner.GetComponent<PlayerInput>();
            if (pi)
            {
                input = pi.actions.FindAction(pointerInput.action.id);
                input.Enable();
            }

        }

        void ReadInputPosition()
        {
            if (input == null)return;
            pointerPosition = input.ReadValue<Vector2>();
        }

        void ConvertInputToWorld()
        {
            placementPosition = cam.ScreenToWorldPoint(new Vector3(pointerPosition.x, pointerPosition.y, -cam.transform.position.z));
            placementPosition.z = 0;
            if (enableGridSnap && gridSize != 0)
                placementPosition = new Vector2(Mathf.Round(placementPosition.x / gridSize) * gridSize,
                    Mathf.Round(placementPosition.y / gridSize) * gridSize);

        }

        void SetHelperPosition()
        {
            if (lookAtHelper == null)return;
            lookAtHelper.Position = placementPosition;
        }

        void CheckPlacement()
        {
            if (PlacementValid())
            {
                if (incorrectSpawn)
                    Destroy(incorrectSpawn);
                if (!correctSpawn)
                    OnCorrectPlacement();

                correctSpawn.transform.position = placementPosition;
                correctSpawn.transform.rotation = placementRotation;
            }
            else
            {
                if (correctSpawn)
                    Destroy(correctSpawn);
                if (!incorrectSpawn)
                    OnIncorrectPlacement();

                incorrectSpawn.transform.position = placementPosition;
                incorrectSpawn.transform.rotation = placementRotation;
            }

        }

        protected virtual void OnCorrectPlacement()
        {
            correctSpawn = Instantiate(correctPlacementPrefab, placementPosition, placementRotation);
        }

        protected virtual void OnIncorrectPlacement()
        {
            incorrectSpawn = Instantiate(incorrectPlacementPrefab, placementPosition, placementRotation);
        }

        protected override void OnItemUse()
        {
            if (PlacementValid())
            {
                SpawnItem();
                if (unselectAllOnCorrectPlacement)
                    owner.UnselectAllEquipped();
            }   
            else if (unselectAllOnIncorrectPlacement)
                owner.UnselectAllEquipped();
        }

        protected override void OnAltItemUse()
        {
            var desired = placementRotation.eulerAngles;
            if (rotateAxis == AxisType.X)
                desired.x += rotateAmount;
            else if (rotateAxis == AxisType.Y)
                desired.y += rotateAmount;
            else
                desired.z += rotateAmount;

            placementRotation = Quaternion.Euler(desired);
        }

        protected override void OnItemCancelUse()
        {
            if (!owner.IsDragging) return;
            OnItemUse();
        }

        protected virtual void SpawnItem()
        {
            if (!PlacementValid())return;
            if (!spawnPrefab || !correctSpawn) return;
            Instantiate(spawnPrefab, correctSpawn.transform.position, correctSpawn.transform.rotation);
            OnItemConsumed();
        }

        void DisplayPreview()
        {
            if (correctSpawn)
            {
                correctSpawn.transform.position = placementPosition;
            }
            else if (incorrectSpawn)
            {
                incorrectSpawn.transform.position = placementPosition;
            }
        }

        public void SetPlacementRotation(Vector3 euler)
        {
            placementRotation = Quaternion.Euler(euler);
        }

        protected bool PlacementValid()
        {
            return !overlapSystem.Detected;
        }

        protected void OnDrawGizmos()
        {
            overlapSystem.DrawDetectable();
        }

        public override void Initialize()
        {
            throw new System.NotImplementedException();
        }

        
    }
}