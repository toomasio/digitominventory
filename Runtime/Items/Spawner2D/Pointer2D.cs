﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using DigitomPhysics;
using UnityEngine;

namespace DigitomInventory
{
    public class Pointer2D : Item
    {
        private LookAtHelper lookAtHelper;

        public override void Initialize() { }

        protected void Awake()
        {
            lookAtHelper = FindObjectOfType<LookAtHelper>();
        }

        private void OnEnable()
        {
            if (lookAtHelper)
                lookAtHelper.isFollowing = false;
        }
    }
}