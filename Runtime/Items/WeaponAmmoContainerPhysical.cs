﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public class WeaponAmmoContainerPhysical : WeaponAmmoContainer
    {
        [SerializeField] protected Projectile[] projectiles;
        [SerializeField] protected bool removeAfterFire;
        [SerializeField] protected bool autoReload;
        [SerializeField] protected float reloadTime;

        protected int curProjectileIndex;

        public override void FireAmmo(ItemWeaponRanged owner, Vector3 direction, float speed)
        {
            projectiles[curProjectileIndex].LaunchProjectile(owner, speed, direction);
        }
    }
}