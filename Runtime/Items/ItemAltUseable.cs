﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class ItemAltUseable : ItemUseable
    {
        [SerializeField] protected UnityEventContainer onAltUseEvents;
        public event System.Action OnAltUse;

        public override void AltUse()
        {
            OnAltItemUse();
            OnAltUse?.Invoke();
            onAltUseEvents.unityEvents?.Invoke();
        }
        protected abstract void OnAltItemUse();

    }
}