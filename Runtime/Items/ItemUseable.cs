﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class ItemUseable : Item
    {
        [SerializeField] protected UnityEventContainer onUseEvents;
        public event System.Action OnUse;

        public override void Use()
        {
            OnItemUse();
            OnUse?.Invoke();
            onUseEvents.unityEvents?.Invoke();
        }
        protected abstract void OnItemUse();

    }
}