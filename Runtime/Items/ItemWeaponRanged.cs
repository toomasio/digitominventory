﻿using System;
using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;

namespace DigitomInventory
{
    public abstract class ItemWeaponRanged : ItemUseable
    {
        [SerializeField] protected Transform muzzle;
        public Transform Muzzle { get { return muzzle; } }
        [SerializeField] protected float projectileSpeed;
        [SerializeField] protected WeaponAmmoContainer ammoContainer;

        public bool FireEnabled { get; protected set; }

        public override void Initialize()
        {
            FireEnabled = true;
        }

        public virtual void EnableFire(bool enable)
        {
            FireEnabled = enable;
        }

        protected override void OnItemUse()
        {
            if (FireEnabled)
                FireWeapon();
        }

        protected abstract void FireWeapon();
    }
}