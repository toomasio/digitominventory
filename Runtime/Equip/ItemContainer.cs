﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomInventory
{
    [Serializable]
    public class ItemContainer
    {
        public Item prefab;
        public Transform parent;
        public bool fillAmountOnStart = true;
        public int overrideStackSize = -1;

        public event Action OnAdd;
        public event Action OnRemove;
        public event Action OnEmpty;
        public event Action OnFull;

        public Item SpawnedItem { get; private set; }
        public bool IsStackable { get { return Size > 0; } }
        public int Size { get; private set; }
        public int CurAmount { get; private set; }
        public bool IsEmpty { get { return CurAmount <= 0; } }
        public bool IsFull { get { return CurAmount >= Size; } }
        private bool initialized;

        public ItemContainer(ItemContainer copy)
        {
            this.prefab = copy.prefab;
            this.parent = copy.parent;
            this.fillAmountOnStart = copy.fillAmountOnStart;
            this.overrideStackSize = copy.overrideStackSize;
        }

        public ItemContainer(Item prefab, Transform parent, bool fillAmountOnStart, int overrideStackSize)
        {
            this.prefab = prefab;
            this.parent = parent;
            this.fillAmountOnStart = fillAmountOnStart;
            this.overrideStackSize = overrideStackSize;
        }

        public void Initialize()
        {
            if (initialized)return;
            SpawnItem();
            if (fillAmountOnStart)
                CurAmount = Size;

            initialized = true;
        }

        void SpawnItem()
        {
            SpawnedItem = UnityEngine.Object.Instantiate(prefab, parent.position, parent.rotation);
            SpawnedItem.transform.SetParent(parent);
            SpawnedItem.Initialize();
            Size = overrideStackSize > -1 ? overrideStackSize : prefab.stackMaxAmount;
        }

        public void Enable()
        {
            if (SpawnedItem)
                SpawnedItem.OnConsumed += Remove;
        }

        public void Disable()
        {
            if (SpawnedItem)
                SpawnedItem.OnConsumed -= Remove;
        }

        public void ActivateItem(bool activate)
        {
            if (SpawnedItem)
                SpawnedItem.gameObject.SetActive(activate);
        }

        private void Remove(Item item)
        {
            Remove(1);
        }

        public void Remove(int amount)
        {
            CurAmount -= amount;
            CheckAmount();
            OnRemove?.Invoke();
        }

        public void Add(ItemContainer item)
        {
            if (!IsStackable)return;
            Add(item.CurAmount);
        }

        public void Add(int amount)
        {
            if (!IsStackable)return;
            if (IsEmpty)
                SpawnItem();
            CurAmount += amount;
            CheckAmount();
            OnAdd?.Invoke();

        }

        public void EmptyAmount()
        {
            if (!IsStackable)return;
            CurAmount = 0;
            CheckAmount();
        }

        public void FillAmount()
        {
            if (!IsStackable)return;
            CurAmount = Size;
            CheckAmount();
        }

        void CheckAmount()
        {
            if (IsEmpty)
                OnIsEmpty();
            else if (IsFull)
                OnIsFull();
        }

        void OnIsEmpty()
        {
            UnityEngine.Object.Destroy(SpawnedItem.gameObject);
            CurAmount = 0;
            OnEmpty?.Invoke();
        }

        void OnIsFull()
        {
            CurAmount = Size;
            OnFull?.Invoke();
        }
    }
}