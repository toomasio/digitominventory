﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DigitomInventory
{
    public class EquipItem : MonoBehaviour
    {
        [SerializeField] protected ItemTag[] allowedTags;
        [SerializeField] protected Transform equipParent;
        [SerializeField] protected ItemContainer[] defaultItems;

        protected List<ItemContainer> curItems = new List<ItemContainer>();
        public ItemContainer CurEquipped { get; protected set; }
        public int CurIndex { get; protected set; }
        public bool IsDragging { get; protected set; }

        protected virtual void Awake()
        {
            Initialize();
        }

        protected virtual void OnDisable()
        {
            for (int i = 0; i < curItems.Count; i++)
            {
                curItems[i].Disable();
            }
        }

        protected virtual void Initialize()
        {
            AddDefaultItems();
            SelectEquipped(CurIndex);
        }

        void AddDefaultItems()
        {
            for (int i = 0; i < defaultItems.Length; i++)
            {
                curItems.Add(new ItemContainer(defaultItems[i]));
                curItems[i].Initialize();
                OnContainerCreated(curItems[i]);
            }

        }

        ItemContainer FindItem(ItemContainer _item)
        {
            for (int i = 0; i < curItems.Count; i++)
            {
                if (_item.prefab == curItems[i].prefab)
                    return curItems[i];
            }
            return null;
        }

        public virtual void Add(ItemContainer value)
        {
            var item = FindItem(value);
            if (item != null)
            {
                item.Add(value);
            }
        }

        protected virtual void OnContainerCreated(ItemContainer container)
        {
            container.SpawnedItem.PickUp(this);
            container.Enable();
        }

        public virtual void Remove(ItemContainer value)
        {
            var item = FindItem(value);
            Destroy(item.SpawnedItem);
            curItems.Remove(item);
        }

        public virtual void SelectEquipped(int index)
        {
            CurIndex = index;
            CurIndex = Mathf.Clamp(CurIndex, 0, curItems.Count - 1);
            for (int i = 0; i < curItems.Count; i++)
                curItems[i].ActivateItem(i == CurIndex);
            UpdateCurEquipped();
        }

        public virtual void UnselectAllEquipped()
        {
            for (int i = 0; i < curItems.Count; i++)
                curItems[i].ActivateItem(false);
        }

        protected virtual void UpdateCurEquipped()
        {
            CurEquipped = curItems[CurIndex];
        }

        public virtual void Next()
        {
            CurIndex++;
            if (CurIndex >= curItems.Count)
                CurIndex = 0;
            SelectEquipped(CurIndex);
        }

        public virtual void Previous()
        {
            CurIndex--;
            if (CurIndex < 0)
                CurIndex = curItems.Count - 1;
            SelectEquipped(CurIndex);
        }

        public virtual void UseItem()
        {
            if (CurEquipped.IsEmpty)return;
            CurEquipped.SpawnedItem.Use();
        }

        public virtual void AltUseItem()
        {
            if (CurEquipped.IsEmpty)return;
            CurEquipped.SpawnedItem.AltUse();
        }

        public virtual void CancelItem()
        {
            if (CurEquipped.IsEmpty)return;
            CurEquipped.SpawnedItem.Cancel();
        }
    }
}