﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInventory
{
    public class UserEquipCanvas : MonoBehaviour
    {
        [SerializeField] private RectTransform parent = null;
        [SerializeField] private UserEquipCanvasSlot slotPrefab = null;
        private List<UserEquipCanvasSlot> curSlots = new List<UserEquipCanvasSlot>();
        private UserEquipCanvasSlot curSlot;
        private void Awake()
        {

        }

        public void AddSlot(ItemContainer itemData, Action<int> onSelectedCallback, 
            Action<int> onDragCallback = null, Action onDropCallback = null, Action onFailDropCallback = null)
        {
            var slot = Instantiate(slotPrefab);
            slot.Initialize(curSlots.Count, itemData, onSelectedCallback, onDragCallback, onDropCallback, onFailDropCallback);
            slot.transform.SetParent(parent);
            curSlots.Add(slot);
        }

        public void RemoveSlot(int index)
        {
            Destroy(curSlots[index].gameObject);
            curSlots.RemoveAt(index);
        }

        public void SelectSlot(int index)
        {
            for (int i = 0; i < curSlots.Count; i++)
            {
                if (i == index)
                {
                    curSlot = curSlots[i];
                    curSlot.Select();
                }

                else
                    curSlots[i].UnSelect();
            }
        }

        public void UnSelectAll()
        {
            for (int i = 0; i < curSlots.Count; i++)
                    curSlots[i].UnSelect();
        }
    }
}