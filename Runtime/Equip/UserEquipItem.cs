﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomInventory
{

    [RequireComponent(typeof(PlayerInput))]
    public class UserEquipItem : EquipItem
    {
        private class SelectedInputHandler
        {
            private readonly int index;
            private readonly InputAction input;
            private readonly Action<int> callback;

            public SelectedInputHandler(int index, PlayerInput playerInput, InputActionReference input, Action<int> callback)
            {
                this.index = index;
                this.input = playerInput.actions.FindAction(input.action.id);
                this.callback = callback;
            }
            public void Enable()
            {
                if (input == null)return;
                input.performed += OnInputSelect;
            }

            public void Disable()
            {
                if (input == null)return;
                input.performed -= OnInputSelect;
            }

            void OnInputSelect(InputAction.CallbackContext ctx)
            {
                callback.Invoke(index);
            }
        }

        [SerializeField] private UserEquipCanvas canvasPrefab = null;
        [SerializeField] private InputActionReference useInput = null;
        private InputAction useInputAction;
        [SerializeField] private InputActionReference altUseInput = null;
        private InputAction altUseInputAction;
        [SerializeField] private InputActionReference nextInput = null;
        private InputAction nextInputAction;
        [SerializeField] private InputActionReference prevInput = null;
        private InputAction prevInputAction;
        [SerializeField] private InputActionReference[] selectInputs = null;
        private SelectedInputHandler[] selectedInputHandlers;

        private PlayerInput playerInput;
        private UserEquipCanvas canvasSpawned;

        protected override void Awake()
        {
            if (canvasPrefab)
                canvasSpawned = Instantiate(canvasPrefab);
            base.Awake();
            playerInput = GetComponent<PlayerInput>();
        }

        private void OnEnable()
        {
            EnableInputHandlers();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            DisableInputHandlers();
        }

        protected override void OnContainerCreated(ItemContainer container)
        {
            base.OnContainerCreated(container);
            if (canvasSpawned != null)
                canvasSpawned.AddSlot(container, SelectEquipped, DragEquipped, UseItem);
        }

        public override void Remove(ItemContainer value)
        {
            base.Remove(value);
            if (canvasSpawned != null)
            {
                canvasSpawned.RemoveSlot(curItems.FindIndex(0, x => x.prefab == value.prefab));
            }

        }

        private void DragEquipped(int index)
        {
            SelectEquipped(index);
            IsDragging = true;
        }

        public override void SelectEquipped(int index)
        {
            base.SelectEquipped(index);
            if (canvasSpawned != null)
                canvasSpawned.SelectSlot(index);
        }

        public override void UnselectAllEquipped()
        {
            base.UnselectAllEquipped();
            if (canvasSpawned != null)
                canvasSpawned.UnSelectAll();
        }

        void EnableInputHandlers()
        {
            if (useInput != null)
            {
                useInputAction = playerInput.actions.FindAction(useInput.action.id);
                useInputAction.Enable();
                useInputAction.performed += OnInputUse;
                useInputAction.canceled += OnInputCancel;
            }
            if (altUseInput != null)
            {
                altUseInputAction = playerInput.actions.FindAction(altUseInput.action.id);
                altUseInputAction.Enable();
                altUseInputAction.performed += OnInputAltUse;
            }

            if (nextInput != null)
            {
                nextInputAction = playerInput.actions.FindAction(nextInput.action.id);
                nextInputAction.Enable();
                nextInputAction.performed += OnInputNext;
            }

            if (prevInput != null)
            {
                prevInputAction = playerInput.actions.FindAction(prevInput.action.id);
                prevInputAction.Enable();
                prevInputAction.performed += OnInputPrev;
            }

            selectedInputHandlers = new SelectedInputHandler[selectInputs.Length];
            for (int i = 0; i < selectedInputHandlers.Length; i++)
            {
                selectedInputHandlers[i] = new SelectedInputHandler(i, playerInput, selectInputs[i], OnInputSelect);
                selectedInputHandlers[i].Enable();
            }
        }

        void DisableInputHandlers()
        {
            if (useInput != null)
            {
                useInputAction.performed -= OnInputUse;
                useInputAction.canceled -= OnInputCancel;
            }
            if (useInput != null)
                altUseInputAction.performed -= OnInputUse;
            //if (axisInput != null)
            //{
            //axisInput.action.performed -= OnInputAxis;
            //}
            if (nextInput != null)
                nextInputAction.performed -= OnInputNext;
            if (prevInput != null)
                prevInputAction.performed -= OnInputPrev;

            for (int i = 0; i < selectedInputHandlers.Length; i++)
                selectedInputHandlers[i].Disable();
        }

        void OnInputUse(InputAction.CallbackContext ctx)
        {
            UseItem();
        }

        void OnInputAltUse(InputAction.CallbackContext ctx)
        {
            AltUseItem();
        }

        void OnInputCancel(InputAction.CallbackContext ctx)
        {
            CancelItem();
            if (IsDragging) IsDragging = false;
        }

        void OnInputNext(InputAction.CallbackContext ctx)
        {
            Next();
        }

        void OnInputPrev(InputAction.CallbackContext ctx)
        {
            Previous();
        }

        void OnInputSelect(int index)
        {
            SelectEquipped(index);
        }
    }
}