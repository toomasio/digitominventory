﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace DigitomInventory
{
    public class UserEquipCanvasSlot : MonoBehaviour, IPointerDownHandler, IPointerClickHandler
    {
        [SerializeField] private Image icon = null;
        [SerializeField] private Text amount = null;
        [SerializeField] private float selectedSizeIncrease = 0.1f;
        [SerializeField] private bool clickable;
        [SerializeField] private bool draggable;
        private int index;
        private ItemContainer item;
        private Action<int> onSelectedCallback;
        private Action<int> onDragCallback;
        private Action onDropCallback;
        private Action onFailDropCallback;

        private RectTransform rectTransform;
        private Vector2 startSize;

        public void Initialize(int index, ItemContainer item, Action<int> onSelectedCallback, 
            Action <int>onDragCallback = null, Action onDropCallback = null, Action onFailDropCallback = null)
        {
            this.index = index;
            this.item = item;
            this.icon.sprite = item.prefab.icon;
            this.onSelectedCallback = onSelectedCallback;
            this.onDragCallback = onDragCallback;
            this.onDropCallback = onDropCallback;
            this.onFailDropCallback = onDropCallback;
            if (item.IsStackable)
            {
                item.OnAdd += UpdateAmount;
                item.OnRemove += UpdateAmount;
                UpdateAmount();
            }
            else
                amount.text = "";
        }

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            startSize = rectTransform.sizeDelta;
        }

        private void OnDisable()
        {
            if (item.IsStackable)
            {
                item.OnAdd -= UpdateAmount;
                item.OnRemove -= UpdateAmount;
            }
        }

        public void Select()
        {
            rectTransform.sizeDelta = startSize * (1 + selectedSizeIncrease);
        }

        public void UnSelect()
        {
            rectTransform.sizeDelta = startSize;
        }

        public void UpdateAmount()
        {
            if (amount == null)return;
            if (!item.IsStackable) { amount.text = ""; return; }
            amount.text = item.CurAmount.ToString();
        }

        public void OnPointerSelect()
        {
            onSelectedCallback.Invoke(index);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!clickable) return;
            onSelectedCallback?.Invoke(index);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!draggable) return;
            onDragCallback?.Invoke(index);
        }
    }
}