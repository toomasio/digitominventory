using UnityEngine;

namespace DigitomInventory
{
    public interface IEquipable<T>
    {
        T CurEquipped { get; }
        int CurIndex { get; }
        void Add(T value);
        void Remove(T value);
        void Next();
        void Previous();
        void SelectEquipped(int index);
    }
}