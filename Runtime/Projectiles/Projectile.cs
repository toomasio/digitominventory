﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;
using DigitomPhysics;

namespace DigitomInventory
{
    public abstract class Projectile : MonoBehaviour
    {
        [SerializeField] protected Transform detectPosition = null;
        [SerializeField] protected LayerMask detectMask = -1;
        [SerializeField] protected int maxHits = 1;
        [SerializeField] protected float detectRadius = 0.25f;
        [SerializeField] protected bool rotateTowardsDirection = true;

        protected float speed;
        public float Speed { get { return speed; } set { speed = value; } }
        protected Vector3 direction;
        public Vector3 Direction { get { return direction; } set { direction = value; } }

        protected Vector3 lastPos;
        protected Vector3 curVelocity;
        protected float curVelocityLength;
        protected Vector3 curDirection;
        protected SphereCast detectCast;
        protected ItemWeaponRanged owner;

        protected bool stopped;
        public bool IsStopped { get { return stopped; } }

        protected virtual void Awake()
        {
            detectCast = new SphereCast(detectPosition, new SphereCastSettings 
            {
                mask = detectMask,
                radius = detectRadius,
                space = Space.World,
                emptyDebugColor = Color.red,
                detectedDebugColor = Color.cyan,
                
            });
        }

        protected virtual void Start()
        {
            lastPos = detectPosition.position;
        }

        protected virtual void FixedUpdate()
        {
            if (stopped)
                return;

            DetectPassThrough();
            detectCast.Tick();
            RotateTowardsDirection();
        }

        protected virtual void OnDrawGizmos()
        {
            if (detectCast != null)
                detectCast.DrawDetectable();
        }

        protected virtual void DetectPassThrough()
        {
            curVelocity = detectPosition.position - lastPos;
            curVelocityLength = curVelocity.magnitude;
            curDirection = curVelocity.normalized;
            var startPos = lastPos - (curDirection * detectCast.Settings.radius);

            detectCast.Settings.direction = curDirection;
            detectCast.Settings.distance = curVelocityLength + detectCast.Settings.radius;

            detectCast.PositionOverride = startPos;

            lastPos = detectPosition.position;
        }

        protected virtual void RotateTowardsDirection()
        {
            if (!rotateTowardsDirection) return;

            if (curDirection != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(curDirection);
        }

        public virtual void LaunchProjectile(ItemWeaponRanged owner, float speed, Vector3 direction)
        {
            this.owner = owner;
            this.speed = speed;
            this.direction = direction;
        }

        public virtual void StopProjectile()
        {
            stopped = true;
        }
    }
}