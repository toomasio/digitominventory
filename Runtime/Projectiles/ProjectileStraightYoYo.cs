﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;
using DigitomPhysics;

namespace DigitomInventory
{
    public class ProjectileStraightYoYo : Projectile
    {
        [SerializeField] protected float maxShootTime = 3f;
        [SerializeField] protected bool snapBackToWeaponMuzzle;
        [SerializeField] protected float snapBackDistance = 0.1f;
        

        protected bool reversing;
        protected float shootTimer;

        protected override void Start()
        {
            base.Start();
            detectCast.OnFirst += DetectCastOnFirst;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            MoveProjectile();
        }

        protected virtual void DetectCastOnFirst(HitInfo obj)
        {
            reversing = true;
        }

        protected virtual void MoveProjectile()
        {
            if (stopped) return;

            shootTimer += Time.deltaTime;
            if (shootTimer > maxShootTime)
            {
                shootTimer = maxShootTime;
                reversing = true;
            }
                
            if (reversing)
            {
                var pos = transform.position;
                var muzzlePos = owner.Muzzle.position;
                direction = (muzzlePos - pos).normalized;
            }

            var move = direction * speed * Time.deltaTime;
            transform.Translate(move, Space.World);

            if (reversing)
            {
                var pos = transform.position;
                var muzzlePos = owner.Muzzle.position;
                var dist = Vector3.Distance(pos, muzzlePos);
                if (dist < snapBackDistance)
                {
                    OnReturn();
                }
            }
        }

        protected virtual void OnReturn()
        {
            if (!snapBackToWeaponMuzzle) return;

            transform.SetParent(owner.Muzzle);
            transform.position = owner.Muzzle.position;
            transform.rotation = owner.Muzzle.rotation;
            reversing = false;
            shootTimer = 0;
            StopProjectile();
        }
    }
}