﻿using System.Collections;
using System.Collections.Generic;
using DigitomEvents;
using UnityEngine;
using DigitomPhysics;

namespace DigitomInventory
{
    public abstract class ProjectileStraight : Projectile
    {
        public override void LaunchProjectile(ItemWeaponRanged owner, float speed, Vector3 direction)
        {
            this.speed = speed;
            this.direction = direction;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            MoveProjectile();
        }

        protected virtual void MoveProjectile()
        {
            var move = direction * speed * Time.deltaTime;
            transform.Translate(move);
        }
    }
}