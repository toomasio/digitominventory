﻿using System.Collections;
using UnityEngine;

namespace DigitomInventory
{
    public enum ItemAimType 
    {
        CameraForward,
        MuzzleForward,
        FromOwner
    }
}